//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebSiteBanHang.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role
    {
        public long Id_Role { get; set; }
        public long Id_User { get; set; }
        public string Name_Level { get; set; }
    
        public virtual User User { get; set; }
    }
}
