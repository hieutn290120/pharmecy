﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSiteBanHang.Models;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc;

namespace WebSiteBanHang.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        PharmecyEntities db = new PharmecyEntities();
        
        public ActionResult Index()
        {
            //Lần lượt tạo các viewbag để lấy list sản phẩm từ cơ sở dữ liệu
            //List Diện điện thoại mới nhất
            var capsule = db.Capsule_Encapsulation_Category;
            //Gán vào ViewBag
            ViewBag.capsule = capsule;

            //List LapTop mới nhất
            var table = db.Table_Category;
            //Gán vào ViewBag
            ViewBag.table = table;

            //List Máy tính bảng mới nhất
            var liqui = db.Liquid_Filling_Category;
            //Gán vào ViewBag
            ViewBag.liqui = liqui;

            return View();
        }

        public ActionResult MenuPartial()
        {
            //Truy vấn lấy về 1 list các sản phẩm
            var listCapsule = db.Capsule_Encapsulation_Category;
            return PartialView(listCapsule);
        }
        [HttpGet]


        [HttpPost]
        public ActionResult DangKy(User tv,FormCollection f)
        {
          
            //Kiểm tra captcha hợp lệ
         
                if (ModelState.IsValid)
                {
                    ViewBag.ThongBao = "Thêm thành công";
                    //Thêm khách hàng vào csdl
                    db.Users.Add(tv);
                    db.SaveChanges();
                }
                else
                { 
                    ViewBag.ThongBao = "Thêm thất bại";
                }
                return View();
            
            return View();
        }
       
        //Xây dựng action đăng nhập
        [HttpPost]
        public ActionResult DangNhap(FormCollection f)
        {
            //Kiểm tra tên đăng nhập và mật khẩu
            string sTaiKhoan = f["txtTenDangNhap"].ToString();
            string sMatKhau = f["txtMatKhau"].ToString();
            User tv = db.Users.SingleOrDefault(n => n.Email == sTaiKhoan && n.Password == sMatKhau);
            if (tv != null)
            {
                Session["TaiKhoan"] = tv;
                return Content("<script>window.location.reload();</script>");
            }
            


            return Content("Tài khoản hoặc mật khẩu không đúng!");
        }

        [HttpGet]
        public ActionResult DangNhap()
        {
            //Kiểm tra tên đăng nhập và mật khẩu
            return View();
        }

        [HttpGet]
        public ActionResult DangKy()
        {
            //Kiểm tra tên đăng nhập và mật khẩu
            return View();
        }

        [HttpGet]
        public ActionResult DangXuat()
        {
            Session["TaiKhoan"] = null;
            return RedirectToAction("Index");
        }
	}
}