﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSiteBanHang.Models;

namespace WebSiteBanHang.Controllers
{
    public class Liquid_Filling_CategoryController : Controller
    {
        private PharmecyEntities db = new PharmecyEntities();

        // GET: Liquid_Filling_Category
        public ActionResult Index()
        {
            var liquid_Filling_Category = db.Liquid_Filling_Category.Include(l => l.Category);
            return View(liquid_Filling_Category.ToList());
        }

        // GET: Liquid_Filling_Category/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = db.Liquid_Filling_Category.Find(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Create
        public ActionResult Create()
        {
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category");
            return View();
        }

        // POST: Liquid_Filling_Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Liquid_Filling,Name,Air_Pressure,Air_Volumn,Filling_Speed,Filling_Range,Id_Category")] Liquid_Filling_Category liquid_Filling_Category)
        {
            if (ModelState.IsValid)
            {
                db.Liquid_Filling_Category.Add(liquid_Filling_Category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = db.Liquid_Filling_Category.Find(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // POST: Liquid_Filling_Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Liquid_Filling,Name,Air_Pressure,Air_Volumn,Filling_Speed,Filling_Range,Id_Category")] Liquid_Filling_Category liquid_Filling_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(liquid_Filling_Category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = db.Liquid_Filling_Category.Find(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            return View(liquid_Filling_Category);
        }

        // POST: Liquid_Filling_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Liquid_Filling_Category liquid_Filling_Category = db.Liquid_Filling_Category.Find(id);
            db.Liquid_Filling_Category.Remove(liquid_Filling_Category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
