﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSiteBanHang.Models;

namespace WebSiteBanHang.Controllers
{
    public class Table_CategoryController : Controller
    {
        private PharmecyEntities db = new PharmecyEntities();

        // GET: Table_Category
        public ActionResult Index()
        {
            var table_Category = db.Table_Category.Include(t => t.Category);
            return View(table_Category.ToList());
        }

        // GET: Table_Category/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table_Category table_Category = db.Table_Category.Find(id);
            if (table_Category == null)
            {
                return HttpNotFound();
            }
            return View(table_Category);
        }

        // GET: Table_Category/Create
        public ActionResult Create()
        {
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category");
            return View();
        }

        // POST: Table_Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Table,Name,Model_Number,Dies,Max_Pressure,Max_Depth,Production_Capacity,Machine_Size,Net_Weight,Id_Category")] Table_Category table_Category)
        {
            if (ModelState.IsValid)
            {
                db.Table_Category.Add(table_Category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", table_Category.Id_Category);
            return View(table_Category);
        }

        // GET: Table_Category/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table_Category table_Category = db.Table_Category.Find(id);
            if (table_Category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", table_Category.Id_Category);
            return View(table_Category);
        }

        // POST: Table_Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Table,Name,Model_Number,Dies,Max_Pressure,Max_Depth,Production_Capacity,Machine_Size,Net_Weight,Id_Category")] Table_Category table_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(table_Category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", table_Category.Id_Category);
            return View(table_Category);
        }

        // GET: Table_Category/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table_Category table_Category = db.Table_Category.Find(id);
            if (table_Category == null)
            {
                return HttpNotFound();
            }
            return View(table_Category);
        }

        // POST: Table_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Table_Category table_Category = db.Table_Category.Find(id);
            db.Table_Category.Remove(table_Category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
